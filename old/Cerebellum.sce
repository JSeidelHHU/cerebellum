scenario = "Cerebellum";
active_buttons = 3;
button_codes = 32,64,128;
#pulse_width = 5
#write_codes = true;
default_font = "Arial";

begin; 

box { height = 150; width = 200; color = 80,20,0; } door;
box { height = 150; width = 200; color = 255,70,0; } open;
picture { text{caption = " "; font_size=48; };x = 0; y = 0; } pic_black;
picture { text{caption = "+"; font_size=48; };x = 0; y = 0; } pic_fix;
picture { text{caption = "Bitte schneller reagieren!"; font_size=36; };x = 0; y = 0;} pic_no_response;
picture { text{caption = "Bitte nicht die Leertaste drücken!"; font_size=36; };x = 0; y = 0;} pic_leertaste;
picture { text{caption = "+20ct"; font_size=48; font_color = 0,204,51; };x = 0; y = 0; } pic_win;
picture { text{caption = "-10ct"; font_size=48; font_color = 255,70,0; };x = 0; y = 0; } pic_loss;
picture { text{caption = " "; font_size=36; font_color = 28,134,238; } text_score; x = 0; y = 0; } pic_score;

array { 
	bitmap { filename = "p1_1.bmp"; } p1A;
	bitmap { filename = "p2_1.bmp"; } p2A;
	bitmap { filename = "p3_1.bmp"; } p3A;
	bitmap { filename = "p4_1.bmp"; } p4A;
		bitmap { filename = "p11_1.bmp"; } p1B;
		bitmap { filename = "p12_1.bmp"; } p2B;
		bitmap { filename = "p13_1.bmp"; } p3B;
		bitmap { filename = "p14_1.bmp"; } p4B;
} bitmaps; 

array { 
	bitmap { filename = "p19_1.bmp"; } p1D;
	bitmap { filename = "p20_1.bmp"; } p2D;
	bitmap { filename = "p21_1.bmp"; } p3D;
	bitmap { filename = "p22_1.bmp"; } p4D;
		bitmap { filename = "p23_1.bmp"; } p1E;
		bitmap { filename = "p24_1.bmp"; } p2E;
		bitmap { filename = "p25_1.bmp"; } p3E;
		bitmap { filename = "p26_1.bmp"; } p4E;
} bitmaps_2;

array {
	bitmap { filename = "p5_1.bmp"; } p1C;
	bitmap { filename = "p6_1.bmp"; } p2C;
} bitmaps_test;

array {
	LOOP $i 11; $nr = '$i+1';
		trial{
			trial_duration = forever;
			trial_type = correct_response;
			picture {bitmap {filename = "Instruktionen/Folie$nr.png"; }; x = 0; y = 0;};
			duration = next_picture;
			target_button = 3;
		};		
	ENDLOOP;
} Instruktionen;

trial {
	picture pic_score;
	duration = 3000;
	target_button = 3;
	code = "Score";
} block_score;

trial {
   picture pic_leertaste;
   duration = 1000;
	target_button = 3;
	code = "Leertaste";
	port_code = 27;
} wrong_response; 

trial {
	picture pic_no_response;
	duration = 1000;
	code = "No_response";
	port_code = 28;
} no_response;

trial {
	picture pic_black;
	duration = 500;
	code = "Delay";
} black;

trial{
	trial_type = correct_response;
	stimulus_event{	
		picture {
			box door; x = -300; y = 0;
			bitmap p1A; x =    0; y = 0;
			box door; x =  300; y = 0;
		} stim;
		target_button = 1,2;
		code = "Stimulus_choice";
	} stimulus_ev; 
	stimulus_event{		
		picture {
			box door; x = -300; y = 0;
			box door; x =  300; y = 0;
		};
		deltat = 492; #8Hz refreshrate
		duration = 492;
		target_button = 1,2;
		code = "Only_choice";
	};
} JustDoors;

array {
	trial{
		picture {
			box door; x = -300; y = 0;
			box open; x = -300; y = 0;
			box door; x =  300; y = 0;
		};
		duration = 500;
		code = "Left_choice";
	};

	trial{
		picture {
			box door; x = -300; y = 0;
			box door; x =  300; y = 0;
			box open; x =  300; y = 0;
		};
		duration = 500;
		code = "Right_choice";
	};
} responses;

array {
	trial {
			picture pic_fix; duration = 500;
			code = "fix500";
	};
	trial {
			picture pic_fix; duration = 600;
			code = "fix600";
	};
	trial {
			picture pic_fix; duration = 700;
			code = "fix700";
	};
	trial {
			picture pic_fix; duration = 800;
			code = "fix800";
	};
	trial {
			picture pic_fix; duration = 900;
			code = "fix900";
	};
	trial {
			picture pic_fix; duration = 1000;
			code = "fix1000";
	};
	trial {
			picture pic_fix; duration = 1100;
			code = "fix1100";
	};
	trial {
			picture pic_fix; duration = 1200;
			code = "fix1200";
	};
	trial {
			picture pic_fix; duration = 1300;
			code = "fix1300";
	};
	trial {
			picture pic_fix; duration = 1400;
			code = "fix1400";
	};
	trial {
			picture pic_fix; duration = 1500;
			code = "fix1500";
	};
} fixation;

array{
	trial {
		picture pic_loss;
		duration = 1000;
		code = "Loss";
		port_code = 0;
	};
	trial {
		picture pic_win;
		duration = 1000;
		code = "Win";
		port_code = 0;
	};
} feedback;

trial{
	nothing {};
	time = 50;
	code = "Prephase_start";
	port_code = 21;
}Pre_phase_start; 

trial{
	nothing {};
	time = 50;
	code = "Prephase_end";
	port_code = 22;
}Pre_phase_end;

trial{
	nothing {};
	time = 50;
	code = "Postphase_end";
	port_code = 23;
}Post_phase_end;

trial{
	nothing {};
	time = 50;
	code = "add_block";
	port_code = 29;
}add_block;

begin_pcl;

array <int> stimuli[56] = {
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		2,2,2,2,2,2,2,2,2,2,2,2,2,2,
		3,3,3,3,3,3,3,3,3,3,3,3,3,3,
		4,4,4,4,4,4,4,4,4,4,4,4,4,4
};

array <int> response_class1[14] = {
		0,0,0,
		1,1,1,
		1,1,1,
		1,1,1,
		1,1
};
	
array <int> response_class2[14] = {
		0,0,0,
		1,1,1,
		1,1,1,
		1,1,1,
		1,1
};

array <int> response_class3[14] = {
		0,0,0,0,0,0,0,
		1,1,1,1,1,1,1
};
	
array <int> response_class4[14] = {
		0,0,0,0,0,0,0,
		1,1,1,1,1,1,1
};

int money = 0; 
int offset = 0; 
bool prephase_ended = false;

array <int> target[4] = {1,1,2,2};
target.shuffle();

int session = parameter_manager.get_int("Session");

if session == 1 then
	bitmaps; 				#stimulus set for session 1
else 
	bitmaps = bitmaps_2; #stimulus set for session 2
end;

bitmaps.shuffle();

int condition = 1;
int delay = parameter_manager.get_int("Delay");	

if delay == 500 then 
	condition = 1;	#500ms, compare markerfile
else 
	condition = 2; #6500ms, compare markerfile
	Pre_phase_start.get_stimulus_event(1).set_port_code(24); #Change Learning Marker for Delay
	Pre_phase_end.get_stimulus_event(1).set_port_code(25);
	Post_phase_end.get_stimulus_event(1).set_port_code(26);
end;

string proband = parameter_manager.get_string("Proband");

output_file behaviour = new output_file(); 
output_file block_behaviour = new output_file();

if file_exists(logfile_directory + proband + "_behaviour.csv") == false then	
	behaviour.open_append(proband + "_behaviour.csv");
	behaviour.print_line("Trial;Block;Wahl;Feedback;Stimulus;Wahrscheinlichkeit;Bedingung;Markercode;Geld;Learning;Reaktionszeit"); 
	block_behaviour.open_append(proband + "_block_behaviour.csv");
	block_behaviour.print_line("Bedingung;Learning_Block"); 
	
	Instruktionen[1].present();
elseif condition == 1 && file_exists(logfile_directory + proband + "_behaviour.csv") == true then
	behaviour.open_append(proband + "_behaviour.csv");
	block_behaviour.open_append(proband + "_block_behaviour.csv");
	
	Instruktionen[10].present();
elseif condition == 2 && file_exists(logfile_directory + proband + "_behaviour.csv") == true then
	behaviour.open_append(proband + "_behaviour.csv");
	block_behaviour.open_append(proband + "_block_behaviour.csv");
	
	Instruktionen[11].present();
else

end;

Instruktionen[2].present();
Instruktionen[3].present();

#	Test block begins
	loop int i = 1 until i > 5 begin
		fixation[random(1,11)].present();  
		stim.set_part(2, bitmaps_test[random(1,2)]); 
		JustDoors.present(); 
		int resp = response_manager.last_response(); 
		if response_manager.hits() > 0 then
			responses[resp].present();
			wait_interval(delay - 8); #8ms refresh rate
			feedback[random(1,2)].present();
		end;
		i = i + 1;
	end;
	
	stimulus_ev.set_port_code(20);
	
	Instruktionen[4].present();
	Instruktionen[5].present();
	
# 	Main block begins
	Pre_phase_start.present();
	loop int j = 1 until j > 6 begin
		stimuli.shuffle(); 
		response_class1.shuffle();
		response_class2.shuffle();
		response_class3.shuffle();
		response_class4.shuffle();
		int class1 = 1, class2 = 1, class3 = 1, class4 = 1; 
		int korrekt = 0;
		
		loop int i = 1 until i > 56 begin
			fixation[random(1,11)].present();
			stim.set_part(2, bitmaps[stimuli[i]+ offset]);
			int time = clock.time();
			JustDoors.present();
			int resp = response_manager.last_response();  
			int stimul = stimuli[i];
			int wahl  = -1;
			int wkeit = -1;
			int feedb = -1;
			int rt    = -1;
			
			if response_manager.incorrects() == 1 then
			elseif response_manager.hits() == 0 then 
			else
				rt = clock.time() - time; 
				responses[resp].present();
				wahl = int(target[stimuli[i]] == resp);
				wait_interval(delay - 8);
				if stimuli[i] == 1 then 
					if response_class1[class1] == 1 then 
						wkeit = 80;		
					else
						wkeit = 20;
					end;
						if wahl == 1 then 
							feedb = response_class1[class1]; 
						else
							feedb = 1 - response_class1[class1]; 
						end;
						
					class1 = class1 + 1; 
				end;
				if stimuli[i] == 2 then 
					if response_class2[class2] == 1 then 
						wkeit = 80;		
					else
						wkeit = 20;
					end;		
						if wahl == 1 then
							feedb = response_class2[class2];
						else
							feedb = 1 - response_class2[class2];	
						end;
					
					class2 = class2 + 1;
				end;
				if stimuli[i] == 3 then 
					wkeit = 50;
					if wahl == 1 then
						feedb = response_class3[class3];
					else
						feedb = 1 - response_class3[class3];
					end;
			
					class3 = class3 + 1;
				end;
				if stimuli[i] == 4 then 
					wkeit = 50;
					if wahl == 1 then
						feedb = response_class4[class4];
					else
						feedb = 1 - response_class4[class4];
					end;
				
					class4 = class4 + 1;
				end;
				
				#Check the learning criteria, counting only for contingency 80:20 hits 
				if wahl == 1 then				
					if stimuli[i] == 1 || stimuli[i] == 2 then # 80/20 class
						korrekt = korrekt + 1;
					end;		
				end;
			end;
			
			if feedb == 1 then 
				money = money + 20; 
			else 
				money = money - 10;
			end;
			
			int mcode = int(wkeit == 50)* 10 + int(feedb == 1)* 4 + wahl * 2 + condition;
			
			if feedb == -1 && response_manager.last_response() == 3 then
				mcode = 27;
				wrong_response.present();
			elseif feedb == -1 && response_manager.incorrects() == 0 then
				mcode = 28;
				no_response.present();
			else
				feedback[feedb + 1].get_stimulus_event(1).set_port_code(mcode);
				feedback[feedb + 1].get_stimulus_event(1).set_event_code(string(mcode));
				feedback[feedb + 1].present();
			end;
			
			#Get the relevant output values for printing
			behaviour.print(i);        behaviour.print(";");
			behaviour.print(j);    		behaviour.print(";");
			behaviour.print(wahl);     behaviour.print(";");
			behaviour.print(feedb);    behaviour.print(";");
			behaviour.print(stimul + offset);   behaviour.print(";");
			behaviour.print(wkeit);    behaviour.print(";");
			behaviour.print(delay);    behaviour.print(";");
			behaviour.print(mcode);    behaviour.print(";");
			behaviour.print(money);    behaviour.print(";");
			behaviour.print(korrekt);  behaviour.print(";");
			behaviour.print(rt);       behaviour.print_line(" ");
			
			i = i + 1;
		end;
		Instruktionen[7].present();
		
		#block_score value
		int korrekt_win = 0;
		korrekt_win = korrekt * 20; # in cent, 28 trials * 20cent = 560 euro max;
		string winwin = string(korrekt_win);
		
		text_score.set_caption("Ihr aktueller Gewinn\n\n" + winwin + " Cent");
		text_score.set_background_color(0,0,0);
		text_score.redraw();
		block_score.present();
	
		Instruktionen[8].present();
		j = j + 1;
		
		block_behaviour.print(delay);    block_behaviour.print(";");
		block_behaviour.print(korrekt);  block_behaviour.print_line(" ");
		
		#Stimulus change, if the criterion was exceeded in block 2, counter j == 3
		if j == 3 && korrekt > 21  then
			Instruktionen[6].present(); 
			offset = 4;
		end;
		
		#Marker Prephase_end == Postphase_start (same marker)
		#Additional block, if learning criteria was exceeded in block 6
		#Giving this add_block a specific marker, only necessary to filter this specific block
		#Creates an ouput for block 6 that is twice as long as usual (112 trials)
		
		if j == 4 && korrekt > 21 && !prephase_ended then
			Pre_phase_end.present();
			prephase_ended = true;
		elseif j == 5 && korrekt > 21 && !prephase_ended then
			Pre_phase_end.present();
			prephase_ended = true;
		elseif j == 6 && korrekt > 21 && !prephase_ended then
			Pre_phase_end.present();
			prephase_ended = true;
		elseif j == 7 && korrekt > 21 && !prephase_ended then
			Pre_phase_end.present();
			prephase_ended = true;
			add_block.present(); 
			j = j - 1;
		else
		end;
	end;
	
Post_phase_end.present();
Instruktionen[9].present(); 
behaviour.close();
block_behaviour.close();